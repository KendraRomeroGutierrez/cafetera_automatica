library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Decodificador_Display is
	generic(var: bit := '0');
	
	port (	entrada_dec: in std_logic_vector(2 downto 0);-- Salida del Selector de estado
			reset: in std_logic;
			salida_dec: out std_logic_vector (7 downto 0));

end Decodificador_Display ;

architecture Behavioral of Decodificador_Display  is
begin
	process(entrada_dec,reset)
	variable norst: std_logic;
	variable salida_i: std_logic_vector (salida_dec'range);
	begin
		norst := not (reset); 
		if norst = '0' then salida_i:= "00000000";   
			elsif entrada_dec="000" then salida_i:="00000001";
			elsif entrada_dec="001" then salida_i:="00000010";
			elsif entrada_dec="010" then salida_i:="00000100";
			elsif entrada_dec="011" then salida_i:="00001000";
			elsif entrada_dec="100" then salida_i:="00010000";  
			elsif entrada_dec="101" then salida_i:="00100000";  
			elsif entrada_dec="110" then salida_i:="01000000";  
			else salida_i:="10000000";
			
		end if;
		if var='0' then salida_dec<=not (salida_i); -- Salida negada displays.
			else salida_dec<= salida_i;
		end if;
	end process;
end Behavioral;
