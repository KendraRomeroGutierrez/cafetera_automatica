-- Create Date: 13.12.2020 
-- Module Name: top - Behavioral
-- Project Name: CAFETERA
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Top is
    Port ( 
        clk: in STD_LOGIC; -- Reloj pinE3
        reset: in STD_LOGIC; -- RESET C12  
        ---------BOTONES--------
        corto: in STD_LOGIC; -- P17
        largo: in STD_LOGIC; -- P18
        americano: in STD_LOGIC; --M17
        Pulsador_on: in STD_LOGIC; -- N17
        
        sugar: in STD_LOGIC; -- Boton para cambiar el nivel del azucar MEJORA
        refill: in STD_LOGIC;   -- MEJORA
        ---------DISPLAY------
        display_number: out STD_LOGIC_VECTOR (6 DOWNTO 0); -- 7 SEGMENTOS
        display_timer_selection: out STD_LOGIC_VECTOR (7 DOWNTO 0); -- 7 SEGMENTOS MAS UN PUNTO
        ---------LEDS------
        bomba: out STD_LOGIC; --
        indica_on: out STD_LOGIC;
        Despensa_led: out STD_LOGIC -- Indica si hay ingredientes(cafes) disponibles
    );
end top;

architecture Behavioral of top is

    component Sincronos is
        Port (
            sinc_in, clk: IN STD_LOGIC;
            sinc_out: OUT STD_LOGIC
        );
    end component;
    component Antirrebote is
        port (
            Pulsador_in1, clk, reset : in std_logic;
            Pulsador_out1 : out std_logic
        );
    end component;
    component Div_Frecuencia is
            generic ( factor: positive:= 8);
            Port ( 
                clk, reset : in STD_LOGIC;
                clk_out : out STD_LOGIC
                );                  
                         
    end component;    
    component Despensa is
        Port ( clk : in STD_LOGIC;
            reset : in STD_LOGIC;
            sugar : in STD_LOGIC;
            active: in STD_LOGIC;
            refill: in STD_LOGIC;
            Despensa_dispo: out STD_LOGIC;
            Despensa_led: out STD_LOGIC;
            sugar_level: out STD_LOGIC_VECTOR (3 DOWNTO 0)
            );
    end component;
    component ME_cafetera is
        port (
            clk : in STD_LOGIC;
            reset : in STD_LOGIC;
            corto : in STD_LOGIC;
            largo : in STD_LOGIC;
            americano : in STD_LOGIC;
            --store : in STD_LOGIC;
            Pulsador_on : in STD_LOGIC;
            time_count: in STD_LOGIC_VECTOR(4 DOWNTO 0);
            bomba: out STD_LOGIC;
            indica_on: out STD_LOGIC;
            time_required: out STD_LOGIC_VECTOR (4 DOWNTO 0);
            active: out STD_LOGIC:='X'
            );
    end component;
    component Temporizador is
        Port ( 
            clk, reset: in STD_LOGIC;
            time_required: in STD_LOGIC_VECTOR (4 DOWNTO 0);
            segment_unit, segment_ten: out STD_LOGIC_VECTOR (3 DOWNTO 0);
            time_count: out STD_LOGIC_VECTOR(4 DOWNTO 0)
            );
    end component;
    component Decodificador is
        Port (
            code : IN std_logic_vector(3 DOWNTO 0);
            led : OUT std_logic_vector(6 DOWNTO 0)
            );
    end component;
    component display_refresh is
        Port ( 
            ten_decoded, unit_decoded, sugar_decoded: in STD_LOGIC_VECTOR (6 DOWNTO 0);
            clk: in STD_LOGIC;
            display_number: out STD_LOGIC_VECTOR (6 DOWNTO 0);
            display_timer_selection: out STD_LOGIC_VECTOR (7 DOWNTO 0)
            );
    end component;


    signal sugar_sync, sugar_rebound: std_logic:='0';
    signal clk_divided, clk_timer: std_logic;
    signal Despensa_dispo: std_logic; --:='1';
    signal time_required: std_logic_vector (4 DOWNTO 0);
    signal time_count: std_logic_vector(4 downto 0);--:= (others => '0');
    signal segment_ten, segment_unit: std_logic_vector(3 downto 0);
    signal ten_decoded, unit_decoded: std_logic_vector(6 downto 0);
    signal active: std_logic;--:='X';
    signal sugar_level: std_logic_vector(3 downto 0):="0101";
    signal sugar_decoded: std_logic_vector(6 downto 0);

begin

    inst_sync_sugar: Sincronos
        port map(
            sinc_in => sugar, 
            clk => clk, 
            sinc_out => sugar_sync
            );

    inst_Antirrebote_Azucar: Antirrebote
        port map(
            Pulsador_in1 => sugar_sync, 
            clk => clk, 
            reset => reset, 
            Pulsador_out1 => sugar_rebound
            );

    inst_Div_Frecuencia_general: Div_Frecuencia 
        generic map( factor => 10000)
        port map( 
            clk => clk, 
            reset => reset, 
            clk_out => clk_divided
            );
    inst_Div_Frecuencia_MaquinaEstado: Div_Frecuencia 
        generic map( factor => 100000000)
        port map( 
            clk => clk, 
            reset => reset, 
            clk_out => clk_timer
            );
     --Deposito
     inst_Despensa: Despensa
        port map(
            clk => clk,
            reset => reset, 
            sugar => sugar_rebound,
            active => active,
            refill => refill,
            Despensa_dispo => Despensa_dispo,
           Despensa_led => Despensa_led,
            sugar_level => sugar_level
            );
     -- Maquina de Estado Cafetera 
     inst_Me_cafetera: ME_cafetera 
        port map(
            clk => clk, 
            reset => reset,
            corto => corto, 
            largo => largo, 
            americano => americano, 
            
            --store => store_available, 
            Pulsador_on => Pulsador_on, 
            time_count => time_count, 
            bomba => bomba, 
            indica_on => indica_on,
            time_required => time_required, 
            active => active
            );
     -- Temporizador 
     inst_Temporizador: Temporizador 
        port map(
        clk => clk_timer, 
        reset => reset, 
        time_required => time_required,
        time_count => time_count, 
        segment_ten => segment_ten,
        segment_unit => segment_unit
        );
     -- Decodificadores 
     inst_Decodificador_decenas: Decodificador
        port map(
            code => segment_ten,
             led => ten_decoded
             );
     inst_Decodificador_unidades: Decodificador  
        port map(
            code => segment_unit, 
            led => unit_decoded
            );
     inst_Decodificador_sugar: Decodificador 
         port map(
             code => sugar_level, 
             led => sugar_decoded
             );
      inst_refresh: display_refresh 
        port map(
            ten_decoded => ten_decoded, 
            unit_decoded => unit_decoded, 
            sugar_decoded => sugar_decoded, 
            clk => clk_divided, 
            display_number => display_number, 
            display_timer_selection => display_timer_selection
            );
   

end Behavioral;
