library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.numeric_std.all;
-- funciona como un contador para poder mostrar todos los display,
-- es el selectror del multiplexo

entity SelectorEstado is
port(
	  clk_SE: in std_logic;-- sera igual a la salida del boque de div_freuencia
	  salida_SE: out std_logic_vector(2 downto 0) --permite seleccionar el estado
	);
end SelectorEstado;

architecture Behavioral of SelectorEstado is

signal contador: natural range 0 to 7:=0;

begin 
	process(clk_SE)
	begin
	   if clk_SE = '1' and clk_SE'event then
			if contador = 0 then salida_SE<="000";
				contador<=contador +1 ;
			elsif contador = 1 then 
				salida_SE<="001";
				contador<=contador +1;
			elsif contador = 2 then
				salida_SE<="010";
				contador<=contador +1;
			elsif contador = 3 then
				salida_SE<="011";
				contador<=contador +1;
			elsif contador = 4 then
				salida_SE<="100";
				contador<=contador +1;
			elsif contador = 5 then
				salida_SE<="101";
				contador<=contador +1;
			elsif contador = 6 then
				salida_SE<="110";
				contador<=contador +1;
			else
				salida_SE<="111";
				contador<=0;		
			end if;
		end if;
	end process;
end behavioral;
