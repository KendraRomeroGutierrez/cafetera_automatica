library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Antirrebote is
    port (
        clk : in std_logic;
        reset : in std_logic;
        Pulsador_in1 : in std_logic;
        Pulsador_out1 : out std_logic
    );
end Antirrebote;

architecture Behavioral of Antirrebote is
    signal Q1, Q2, Q3 : std_logic;
begin
    process(clk)
    begin
        if rising_edge(clk) then
            if ( reset = '0') then
                Q1 <= '0';
                Q2 <= '0';
                Q3 <= '0';
            else
                Q1 <= Pulsador_in1;
                Q2 <= Q1;
                Q3 <= Q2;
            end if;
        end if;
    end process;
   Pulsador_out1  <= Q1 and Q2 and (not Q3);

end Behavioral;