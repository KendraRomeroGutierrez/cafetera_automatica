
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity Despensa is
    generic (capacity: positive:=5);
    Port ( clk : in STD_LOGIC; 
           reset : in STD_LOGIC;
           sugar : in STD_LOGIC;
           active: in STD_LOGIC;
           refill: in STD_LOGIC;
           Despensa_dispo: out STD_LOGIC;
           Despensa_led: out STD_LOGIC;
           sugar_level: out STD_LOGIC_VECTOR (3 DOWNTO 0)
    );
end Despensa;

architecture Behavioral of Despensa is
begin
    process( clk, reset) 
    variable aux_level: positive range 1 to 5 := 3; -- nivel de azucar        
    variable available: natural range 0 to 50 := capacity; -- ingredientes
    begin
        if reset = '0' then
            aux_level := 3;
            available := capacity; -- Se rellenan los ingredientes
             Despensa_dispo <= '1';
            Despensa_led <= '0';
        elsif rising_edge(clk) then
            if refill = '1' then
                Despensa_led <= '0';
                available := capacity; -- Se rellenan los ingredientes
            elsif sugar = '1' then
                if aux_level < 5 then
                    aux_level := aux_level + 1;
                else
                    aux_level := 1;
                end if;
            end if;
        end if;
        if rising_edge(active) then
            if available > 1 then
                available := available - 1;
                 Despensa_dispo <= '1';
               Despensa_led <= '0';
            else 
                 Despensa_dispo <= '0';
                Despensa_led <= '1';
            end if;
        end if;
        sugar_level <= conv_std_logic_vector(aux_level, 4);
    end process;
    
end Behavioral;