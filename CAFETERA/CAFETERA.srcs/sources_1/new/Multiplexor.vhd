library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity  Multiplexor is
	-- Permite mostrar las opciones seleccionadas en ME_Cafetera
	port(	display1: in std_logic_vector (7 downto 0);
			display2: in std_logic_vector (7 downto 0);
			display3: in std_logic_vector (7 downto 0);
			display4: in std_logic_vector (7 downto 0);
			display5: in std_logic_vector (7 downto 0);
			display6: in std_logic_vector (7 downto 0);
			display7: in std_logic_vector (7 downto 0);
			display8: in std_logic_vector (7 downto 0);
			selector: in std_logic_vector(2 downto 0); -- salida del SelectorEstado
			caracter: out std_logic_vector (7 downto 0));

end Multiplexor;

architecture behavioral of Multiplexor is
	begin
		process(display1,display2,display3,display4,display5,display6,display7,display8,selector)
		begin
			case selector iS
				when "000" => caracter <= display1;
				when "001" => caracter <= display2;
				when "010" => caracter <= display3;
				when "011" => caracter <= display4;
				when "100" => caracter <= display5;
				when "101" => caracter <= display6;
				when "110" => caracter <= display7;
				when others => caracter <= display8;
			end case;
		end process;
end behavioral;