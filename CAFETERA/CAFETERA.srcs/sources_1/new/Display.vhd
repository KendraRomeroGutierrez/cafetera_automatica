library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Display is
	port(	display1_D,display2_D,display3_D,display4_D, display5_D,display6_D,display7_D,display8_D: in std_logic_vector(7 downto 0);
			clk: in std_logic;
			reset: in std_logic;
			caracter_D: out std_logic_vector(7 downto 0);
			control_display: out std_logic_vector(7 downto 0));
end Display;

architecture Behavioral of Display is

	component Multiplexor
		port(	display1: in std_logic_vector (7 downto 0);
			display2: in std_logic_vector (7 downto 0);
			display3: in std_logic_vector (7 downto 0);
			display4: in std_logic_vector (7 downto 0);
			display5: in std_logic_vector (7 downto 0);
			display6: in std_logic_vector (7 downto 0);
			display7: in std_logic_vector (7 downto 0);
			display8: in std_logic_vector (7 downto 0);
			selector: in std_logic_vector(2 downto 0); -- salida del SelectorEstado
			caracter: out std_logic_vector (7 downto 0));
	end component;
	
	component  Decodificador_Display
	port (	entrada_dec: in std_logic_vector(2 downto 0);-- Salida del Selector de estado
			reset: in std_logic;
			salida_dec: out std_logic_vector (7 downto 0));
	end component;
	
	component SelectorEstado is
    port(
	    clk_SE: in std_logic;-- sera igual a la salida del boque de div_freuencia
	   salida_SE: out std_logic_vector(2 downto 0) --permite seleccionar el estado
	);
    end component;
	
	component Div_Frecuencia is
    Generic (frec: integer:=5);  -- Por defecto 
    Port ( 
           clk : in  STD_LOGIC; -- reloj
           reset : in  STD_LOGIC; -- reset 
           clk_out : out  STD_LOGIC); -- pulso con duracion de un ciclo
    end component;
	
-- USOS
for all: Multiplexor use entity work.Multiplexor(Behavioral);
for all: Decodificador_Display use entity work.Decodificador_Display(Behavioral);
for all: SelectorEstado use entity work.SelectorEstado(Behavioral);
for all: Div_Frecuencia use entity work.Div_Frecuencia(Behavioral);
--- Señales---

signal selector_s: std_logic_vector(1 downto 0);
signal clk_div_int: std_logic;


begin
--INSTANCIACIÓN
C_Multiplexor: Multiplexor PORT MAP(
		display1 => display1_D,-- salida de ME_cafetera
		display2 => display2_D,-- salida de ME_cafetera
		display3 => display3_D,-- salida de ME_cafetera
		display4 => display4_D,-- salida de ME_cafetera
		display5 => display5_D,-- salida de ME_cafetera
		display6 => display6_D,-- salida de ME_cafetera
		display7 => display7_D,-- salida de ME_cafetera
		display8 => display8_D,-- salida de ME_cafetera
		selector => selector_S,-- salida de SelectorEstado
		caracter => caracter_D -- Salida del display
	); 

C_Decodificador: Decodificador_Display PORT MAP(
		entrada_dec => selector_s,
		reset => '0',
		salida_dec => control_display
	);
	
C_SelectorEstado: SelectorEstado PORT MAP( 
		clk_SE => clk_div_int,
		salida_SE => selector_s 
	);
	
C_Div_Frecuencia : Div_Frecuencia 
	generic map( frec => 50000)
	PORT MAP(
		clk => clk,
		reset => '0',
		clk_out => clk_div_int
	);


end Behavioral;
