library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;

entity ME_cafetera is
    Port ( clk : in STD_LOGIC;
           reset : in STD_LOGIC;
           corto : in STD_LOGIC;
           largo : in STD_LOGIC;
           americano : in STD_LOGIC;
           --store : in STD_LOGIC;
           Pulsador_on : in STD_LOGIC;
           time_count: in STD_LOGIC_VECTOR (4 DOWNTO 0);
           bomba: out STD_LOGIC;
           indica_on: out STD_LOGIC;
           time_required: out STD_LOGIC_VECTOR (4 DOWNTO 0);
           active: out STD_LOGIC
    );
end ME_cafetera;

architecture Behavioral of ME_cafetera is
    signal time_count_i: unsigned (4 downto 0):= unsigned(time_count);
begin
    indica_on <= Pulsador_on;
    process(clk, reset, time_count_i)
        variable time_req: natural := 0;
    begin
    time_req := 0;
    time_count_i <= unsigned(time_count);
    active <= '0';
        if reset = '0' then
            bomba <= '0';
            active <= '0';
        elsif rising_edge(clk) then
            --if store = '1' then
                if Pulsador_on = '1' then
                    if corto = '1' then
                        time_req := 10;
                    elsif largo = '1' then
                        time_req := 20;
                    elsif americano = '1' then
                        time_req := 15;
                    end if;
                end if;
            --end if;
        end if;
        time_required <= conv_std_logic_vector(time_req, 5);
        if time_count_i > 0 then
            active <= '1';
            bomba <= '1';
        else 
            active <= '0';
            bomba <= '0';
        end if;
        
    end process;
    
end Behavioral;
