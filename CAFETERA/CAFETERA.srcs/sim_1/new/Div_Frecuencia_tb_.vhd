--Testbench del divisor de reloj
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity Div_Frecuencia_tb  is 
end Div_Frecuencia_tb ;

architecture behavioral of Div_Frecuencia_tb is
component Div_Frecuencia is 
	port(	clk: in std_logic;  --Entrada reloj original
	        reset: in std_logic;  --Entrada reset
			clk_out: out std_logic);  --Salida reloj dividido
end component;

--Una se�al para cada puerto del componente
signal clk_s: std_logic :='0';  --Se�al clock se inicializa en 0
signal reset_s: std_logic :='0';  --Se�al reset se inicializa en 0
signal clk_out_s: std_logic;  --Se�al reloj dividido

begin
    -- Mapeo de Se�ales
  UUT: Div_Frecuencia port map(  --Asignaci�n de puertos a las se�ales correspondientes
	clk => clk_s,   
	reset => reset_s,
	clk_out => clk_out_s);  
  --Generaci�n de est�mulos
  reset_s <='1', '0' after 60 ns;
  clk_s <= not clk_s after 10 ns;

end behavioral;
