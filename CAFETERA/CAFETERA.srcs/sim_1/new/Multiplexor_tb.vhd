
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL; 

entity Multiplexor_tb is
end Multiplexor_tb;

architecture Behavioral of Multiplexor_tb is

component Multiplexor is
port(	    display1: in std_logic_vector (7 downto 0);
			display2: in std_logic_vector (7 downto 0);
			display3: in std_logic_vector (7 downto 0);
			display4: in std_logic_vector (7 downto 0);
			display5: in std_logic_vector (7 downto 0);
			display6: in std_logic_vector (7 downto 0);
			display7: in std_logic_vector (7 downto 0);
			display8: in std_logic_vector (7 downto 0);
			selector: in std_logic_vector(2 downto 0); -- salida del SelectorEstado
			caracter: out std_logic_vector (7 downto 0));
end component;  

-- probaremos que ME_CAFETERA PASA EL ESTADO 00 el cual muestra el mesnaje de CAFETERA
            signal display1_s : std_logic_vector(7 downto 0); -- C
            signal display2_s : std_logic_vector(7 downto 0); -- A
            signal display3_s : std_logic_vector(7 downto 0); -- F
            signal display4_s : std_logic_vector(7 downto 0); -- E
            signal display5_s : std_logic_vector(7 downto 0); -- t
            signal display6_s : std_logic_vector(7 downto 0); -- E
            signal display7_s : std_logic_vector(7 downto 0); -- r
            signal display8_s : std_logic_vector(7 downto 0); -- A
            signal selector_s: std_logic_vector (2 downto 0); -- contador en 000
            signal caracter_s: std_logic_vector (7 downto 0);
begin
   UUT: Multiplexor port map(
   display1 => display1_s,
   display2 => display2_s,
   display3 => display3_s,
   display4 => display4_s,
   display5 => display5_s,
   display6 => display6_s,
   display7 => display7_s,
   display8 => display8_s,
   selector => selector_s,
   caracter => caracter_s);
   
   selector_prueba :process
   begin
		selector_S <= "000";
		wait for 10ns;
		selector_S <= "001";
		wait for 10ns;
		selector_S <= "010";
		wait for 10ns;
		selector_S <= "011";
		wait for 10ns;
		selector_S <= "100";
		wait for 10ns;
		selector_S <= "101";
		wait for 10ns;
		selector_S <= "110";
		wait for 10ns;
		selector_S <= "111";
		wait for 10ns;
   end process;
   
   Salida_ME_Cafetera: process
   begin		

		wait for 50 ns; -- comprobamos que se forme la palabra Cafetera
		display1_s<= "11000110"; --C
		display2_s<= "10001000"; --A
		display3_s<= "10001110"; --F
		display4_s<= "10000110"; --E
		display5_s<= "10000111"; --t
		display6_s<= "10000110"; --E
		display7_s<= "10101111"; --r
		display8_s<= "10001000"; --A
      wait;
      
      end process;

end Behavioral;
