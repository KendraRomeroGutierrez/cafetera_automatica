-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- --------------------------------------------------------------------------------
-- Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
-- Date        : Wed Dec 23 13:58:33 2020
-- Host        : DESKTOP-2OH0L8E running 64-bit major release  (build 9200)
-- Command     : write_vhdl -mode funcsim -nolib -force -file
--               C:/Users/kendr/OneDrive/Documentos/cafetera_automatica/CAFETERA/CAFETERA.sim/sim_1/synth/func/xsim/Display_tb_func_synth.vhd
-- Design      : Top
-- Purpose     : This VHDL netlist is a functional simulation representation of the design and should not be modified or
--               synthesized. This netlist cannot be used for SDF annotated simulation.
-- Device      : xc7k70tfbv676-1
-- --------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Antirrebote is
  port (
    Q1 : out STD_LOGIC;
    Q2 : out STD_LOGIC;
    Q3 : out STD_LOGIC;
    aux_level : out STD_LOGIC;
    Q1_reg_0 : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    reset_IBUF : in STD_LOGIC
  );
end Antirrebote;

architecture STRUCTURE of Antirrebote is
  signal \^q1\ : STD_LOGIC;
  signal \^q2\ : STD_LOGIC;
  signal Q2_i_1_n_0 : STD_LOGIC;
  signal \^q3\ : STD_LOGIC;
  signal Q3_i_1_n_0 : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of Q3_i_1 : label is "soft_lutpair0";
  attribute SOFT_HLUTNM of \aux_level[2]_i_2\ : label is "soft_lutpair0";
begin
  Q1 <= \^q1\;
  Q2 <= \^q2\;
  Q3 <= \^q3\;
Q1_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => Q1_reg_0,
      Q => \^q1\,
      R => '0'
    );
Q2_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q1\,
      I1 => reset_IBUF,
      O => Q2_i_1_n_0
    );
Q2_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => Q2_i_1_n_0,
      Q => \^q2\,
      R => '0'
    );
Q3_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => \^q2\,
      I1 => reset_IBUF,
      O => Q3_i_1_n_0
    );
Q3_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => Q3_i_1_n_0,
      Q => \^q3\,
      R => '0'
    );
\aux_level[2]_i_2\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => \^q2\,
      I1 => \^q3\,
      I2 => \^q1\,
      I3 => SR(0),
      O => aux_level
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Despensa is
  port (
    Despensa_led_OBUF : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 3 downto 0 );
    \aux_level_reg[0]_0\ : out STD_LOGIC;
    \FSM_onehot_state_reg[0]\ : out STD_LOGIC;
    \FSM_onehot_state_reg[0]_0\ : out STD_LOGIC;
    bomba_OBUF : in STD_LOGIC;
    \display_number_reg[5]\ : in STD_LOGIC;
    \display_number_reg[5]_0\ : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \display_number_reg[3]\ : in STD_LOGIC;
    \display_number_reg[4]\ : in STD_LOGIC;
    \display_number_reg[4]_0\ : in STD_LOGIC;
    \display_number_reg[1]\ : in STD_LOGIC;
    \display_number_reg[2]\ : in STD_LOGIC;
    \display_number_reg[2]_0\ : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    SR : in STD_LOGIC_VECTOR ( 0 to 0 );
    clk_IBUF_BUFG : in STD_LOGIC;
    aux_level : in STD_LOGIC;
    Q2 : in STD_LOGIC;
    Q3 : in STD_LOGIC;
    Q1 : in STD_LOGIC;
    clear : in STD_LOGIC
  );
end Despensa;

architecture STRUCTURE of Despensa is
  signal Despensa_led_i_1_n_0 : STD_LOGIC;
  signal \aux_level[0]_i_1_n_0\ : STD_LOGIC;
  signal \aux_level[1]_i_1_n_0\ : STD_LOGIC;
  signal \aux_level[2]_i_1_n_0\ : STD_LOGIC;
  signal \^aux_level_reg[0]_0\ : STD_LOGIC;
  signal \aux_level_reg_n_0_[0]\ : STD_LOGIC;
  signal \aux_level_reg_n_0_[1]\ : STD_LOGIC;
  signal \aux_level_reg_n_0_[2]\ : STD_LOGIC;
  signal \available[0]_i_1_n_0\ : STD_LOGIC;
  signal \available[1]_i_1_n_0\ : STD_LOGIC;
  signal \available[2]_i_1_n_0\ : STD_LOGIC;
  signal \available[3]_i_1_n_0\ : STD_LOGIC;
  signal \available[4]_i_1_n_0\ : STD_LOGIC;
  signal \available[4]_i_2_n_0\ : STD_LOGIC;
  signal available_reg : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal \available_reg_n_0_[0]\ : STD_LOGIC;
  signal \available_reg_n_0_[1]\ : STD_LOGIC;
  signal \available_reg_n_0_[2]\ : STD_LOGIC;
  signal \available_reg_n_0_[3]\ : STD_LOGIC;
  signal \available_reg_n_0_[4]\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \available[2]_i_1\ : label is "soft_lutpair1";
  attribute SOFT_HLUTNM of \available[3]_i_1\ : label is "soft_lutpair1";
begin
  \aux_level_reg[0]_0\ <= \^aux_level_reg[0]_0\;
Despensa_led_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFD"
    )
        port map (
      I0 => reset_IBUF,
      I1 => available_reg(1),
      I2 => available_reg(2),
      I3 => available_reg(3),
      I4 => available_reg(4),
      O => Despensa_led_i_1_n_0
    );
Despensa_led_reg: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => bomba_OBUF,
      CE => '1',
      D => '1',
      Q => Despensa_led_OBUF,
      R => Despensa_led_i_1_n_0
    );
\aux_level[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFBFF00000C00"
    )
        port map (
      I0 => \aux_level_reg_n_0_[2]\,
      I1 => Q2,
      I2 => Q3,
      I3 => Q1,
      I4 => SR(0),
      I5 => \aux_level_reg_n_0_[0]\,
      O => \aux_level[0]_i_1_n_0\
    );
\aux_level[1]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1F40"
    )
        port map (
      I0 => \aux_level_reg_n_0_[2]\,
      I1 => \aux_level_reg_n_0_[0]\,
      I2 => aux_level,
      I3 => \aux_level_reg_n_0_[1]\,
      O => \aux_level[1]_i_1_n_0\
    );
\aux_level[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"1F80"
    )
        port map (
      I0 => \aux_level_reg_n_0_[0]\,
      I1 => \aux_level_reg_n_0_[1]\,
      I2 => aux_level,
      I3 => \aux_level_reg_n_0_[2]\,
      O => \aux_level[2]_i_1_n_0\
    );
\aux_level_reg[0]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \aux_level[0]_i_1_n_0\,
      PRE => clear,
      Q => \aux_level_reg_n_0_[0]\
    );
\aux_level_reg[1]\: unisim.vcomponents.FDPE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \aux_level[1]_i_1_n_0\,
      PRE => clear,
      Q => \aux_level_reg_n_0_[1]\
    );
\aux_level_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => clear,
      D => \aux_level[2]_i_1_n_0\,
      Q => \aux_level_reg_n_0_[2]\
    );
\available[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4444444444444448"
    )
        port map (
      I0 => available_reg(0),
      I1 => reset_IBUF,
      I2 => available_reg(1),
      I3 => available_reg(2),
      I4 => available_reg(3),
      I5 => available_reg(4),
      O => \available[0]_i_1_n_0\
    );
\available[1]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"90"
    )
        port map (
      I0 => available_reg(0),
      I1 => available_reg(1),
      I2 => reset_IBUF,
      O => \available[1]_i_1_n_0\
    );
\available[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AF9F"
    )
        port map (
      I0 => available_reg(2),
      I1 => available_reg(1),
      I2 => reset_IBUF,
      I3 => available_reg(0),
      O => \available[2]_i_1_n_0\
    );
\available[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"A0A0A090"
    )
        port map (
      I0 => available_reg(3),
      I1 => available_reg(0),
      I2 => reset_IBUF,
      I3 => available_reg(1),
      I4 => available_reg(2),
      O => \available[3]_i_1_n_0\
    );
\available[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"00010000"
    )
        port map (
      I0 => available_reg(4),
      I1 => available_reg(3),
      I2 => available_reg(2),
      I3 => available_reg(1),
      I4 => reset_IBUF,
      O => \available[4]_i_1_n_0\
    );
\available[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"AA00AA00AA00A900"
    )
        port map (
      I0 => available_reg(4),
      I1 => available_reg(2),
      I2 => available_reg(1),
      I3 => reset_IBUF,
      I4 => available_reg(0),
      I5 => available_reg(3),
      O => \available[4]_i_2_n_0\
    );
\available_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => bomba_OBUF,
      CE => '1',
      D => \available[0]_i_1_n_0\,
      Q => \available_reg_n_0_[0]\,
      R => '0'
    );
\available_reg[0]__0\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \available_reg_n_0_[0]\,
      Q => available_reg(0),
      S => SR(0)
    );
\available_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => bomba_OBUF,
      CE => '1',
      D => \available[1]_i_1_n_0\,
      Q => \available_reg_n_0_[1]\,
      R => \available[4]_i_1_n_0\
    );
\available_reg[1]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \available_reg_n_0_[1]\,
      Q => available_reg(1),
      R => SR(0)
    );
\available_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => bomba_OBUF,
      CE => '1',
      D => \available[2]_i_1_n_0\,
      Q => \available_reg_n_0_[2]\,
      R => \available[4]_i_1_n_0\
    );
\available_reg[2]__0\: unisim.vcomponents.FDSE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \available_reg_n_0_[2]\,
      Q => available_reg(2),
      S => SR(0)
    );
\available_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => bomba_OBUF,
      CE => '1',
      D => \available[3]_i_1_n_0\,
      Q => \available_reg_n_0_[3]\,
      R => \available[4]_i_1_n_0\
    );
\available_reg[3]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \available_reg_n_0_[3]\,
      Q => available_reg(3),
      R => SR(0)
    );
\available_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => bomba_OBUF,
      CE => '1',
      D => \available[4]_i_2_n_0\,
      Q => \available_reg_n_0_[4]\,
      R => \available[4]_i_1_n_0\
    );
\available_reg[4]__0\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => \available_reg_n_0_[4]\,
      Q => available_reg(4),
      R => SR(0)
    );
\display_number[0]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F4444F4F44444444"
    )
        port map (
      I0 => \display_number_reg[1]\,
      I1 => Q(0),
      I2 => \aux_level_reg_n_0_[2]\,
      I3 => \aux_level_reg_n_0_[0]\,
      I4 => \aux_level_reg_n_0_[1]\,
      I5 => Q(1),
      O => \FSM_onehot_state_reg[0]_0\
    );
\display_number[1]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"F8888888FFF88888"
    )
        port map (
      I0 => \display_number_reg[1]\,
      I1 => Q(0),
      I2 => \aux_level_reg_n_0_[0]\,
      I3 => \aux_level_reg_n_0_[1]\,
      I4 => Q(1),
      I5 => \aux_level_reg_n_0_[2]\,
      O => \FSM_onehot_state_reg[0]\
    );
\display_number[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEEFEAAAA"
    )
        port map (
      I0 => \display_number_reg[2]\,
      I1 => \aux_level_reg_n_0_[0]\,
      I2 => \aux_level_reg_n_0_[2]\,
      I3 => \aux_level_reg_n_0_[1]\,
      I4 => Q(1),
      I5 => \display_number_reg[2]_0\,
      O => D(0)
    );
\display_number[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFEAAAAAAA"
    )
        port map (
      I0 => \display_number_reg[3]\,
      I1 => \aux_level_reg_n_0_[2]\,
      I2 => \aux_level_reg_n_0_[0]\,
      I3 => \aux_level_reg_n_0_[1]\,
      I4 => Q(1),
      I5 => \^aux_level_reg[0]_0\,
      O => D(1)
    );
\display_number[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFAAAABAAA"
    )
        port map (
      I0 => \display_number_reg[4]\,
      I1 => \aux_level_reg_n_0_[0]\,
      I2 => \aux_level_reg_n_0_[1]\,
      I3 => Q(1),
      I4 => \aux_level_reg_n_0_[2]\,
      I5 => \display_number_reg[4]_0\,
      O => D(2)
    );
\display_number[5]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"4FF4444444444444"
    )
        port map (
      I0 => \display_number_reg[5]\,
      I1 => \display_number_reg[5]_0\,
      I2 => \aux_level_reg_n_0_[1]\,
      I3 => \aux_level_reg_n_0_[0]\,
      I4 => \aux_level_reg_n_0_[2]\,
      I5 => Q(1),
      O => D(3)
    );
\display_number[6]_i_6\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"44444FF444444444"
    )
        port map (
      I0 => \display_number_reg[1]\,
      I1 => \display_number_reg[2]_0\,
      I2 => \aux_level_reg_n_0_[0]\,
      I3 => \aux_level_reg_n_0_[2]\,
      I4 => \aux_level_reg_n_0_[1]\,
      I5 => Q(1),
      O => \^aux_level_reg[0]_0\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Div_Frecuencia is
  port (
    clk_divided : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC
  );
end Div_Frecuencia;

architecture STRUCTURE of Div_Frecuencia is
  signal \^clk_divided\ : STD_LOGIC;
  signal clk_sig_i_1_n_0 : STD_LOGIC;
  signal cnt : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \cnt[31]_i_10_n_0\ : STD_LOGIC;
  signal \cnt[31]_i_2_n_0\ : STD_LOGIC;
  signal \cnt[31]_i_3_n_0\ : STD_LOGIC;
  signal \cnt[31]_i_4_n_0\ : STD_LOGIC;
  signal \cnt[31]_i_5_n_0\ : STD_LOGIC;
  signal \cnt[31]_i_7_n_0\ : STD_LOGIC;
  signal \cnt[31]_i_8_n_0\ : STD_LOGIC;
  signal \cnt[31]_i_9_n_0\ : STD_LOGIC;
  signal cnt_0 : STD_LOGIC_VECTOR ( 31 downto 0 );
  signal \cnt_reg[12]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[12]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[12]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[12]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_reg[16]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[16]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[16]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[16]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_reg[20]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[20]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[20]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[20]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_reg[24]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[24]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[24]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[24]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_reg[28]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[28]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[28]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[28]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_reg[31]_i_6_n_2\ : STD_LOGIC;
  signal \cnt_reg[31]_i_6_n_3\ : STD_LOGIC;
  signal \cnt_reg[4]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[4]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[4]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[4]_i_2_n_3\ : STD_LOGIC;
  signal \cnt_reg[8]_i_2_n_0\ : STD_LOGIC;
  signal \cnt_reg[8]_i_2_n_1\ : STD_LOGIC;
  signal \cnt_reg[8]_i_2_n_2\ : STD_LOGIC;
  signal \cnt_reg[8]_i_2_n_3\ : STD_LOGIC;
  signal data0 : STD_LOGIC_VECTOR ( 31 downto 1 );
  signal \NLW_cnt_reg[31]_i_6_CO_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 downto 2 );
  signal \NLW_cnt_reg[31]_i_6_O_UNCONNECTED\ : STD_LOGIC_VECTOR ( 3 to 3 );
  attribute ADDER_THRESHOLD : integer;
  attribute ADDER_THRESHOLD of \cnt_reg[12]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cnt_reg[16]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cnt_reg[20]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cnt_reg[24]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cnt_reg[28]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cnt_reg[31]_i_6\ : label is 35;
  attribute ADDER_THRESHOLD of \cnt_reg[4]_i_2\ : label is 35;
  attribute ADDER_THRESHOLD of \cnt_reg[8]_i_2\ : label is 35;
begin
  clk_divided <= \^clk_divided\;
clk_sig_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0001"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => \^clk_divided\,
      O => clk_sig_i_1_n_0
    );
clk_sig_reg: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => clk_sig_i_1_n_0,
      Q => \^clk_divided\
    );
\cnt[0]_i_1\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => cnt(0),
      O => cnt_0(0)
    );
\cnt[10]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(10),
      O => cnt_0(10)
    );
\cnt[11]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(11),
      O => cnt_0(11)
    );
\cnt[12]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(12),
      O => cnt_0(12)
    );
\cnt[13]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(13),
      O => cnt_0(13)
    );
\cnt[14]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(14),
      O => cnt_0(14)
    );
\cnt[15]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(15),
      O => cnt_0(15)
    );
\cnt[16]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(16),
      O => cnt_0(16)
    );
\cnt[17]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(17),
      O => cnt_0(17)
    );
\cnt[18]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(18),
      O => cnt_0(18)
    );
\cnt[19]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(19),
      O => cnt_0(19)
    );
\cnt[1]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(1),
      O => cnt_0(1)
    );
\cnt[20]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(20),
      O => cnt_0(20)
    );
\cnt[21]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(21),
      O => cnt_0(21)
    );
\cnt[22]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(22),
      O => cnt_0(22)
    );
\cnt[23]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(23),
      O => cnt_0(23)
    );
\cnt[24]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(24),
      O => cnt_0(24)
    );
\cnt[25]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(25),
      O => cnt_0(25)
    );
\cnt[26]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(26),
      O => cnt_0(26)
    );
\cnt[27]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(27),
      O => cnt_0(27)
    );
\cnt[28]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(28),
      O => cnt_0(28)
    );
\cnt[29]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(29),
      O => cnt_0(29)
    );
\cnt[2]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(2),
      O => cnt_0(2)
    );
\cnt[30]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(30),
      O => cnt_0(30)
    );
\cnt[31]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(31),
      O => cnt_0(31)
    );
\cnt[31]_i_10\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt(13),
      I1 => cnt(12),
      I2 => cnt(15),
      I3 => cnt(14),
      O => \cnt[31]_i_10_n_0\
    );
\cnt[31]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt(18),
      I1 => cnt(19),
      I2 => cnt(16),
      I3 => cnt(17),
      I4 => \cnt[31]_i_7_n_0\,
      O => \cnt[31]_i_2_n_0\
    );
\cnt[31]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt(26),
      I1 => cnt(27),
      I2 => cnt(24),
      I3 => cnt(25),
      I4 => \cnt[31]_i_8_n_0\,
      O => \cnt[31]_i_3_n_0\
    );
\cnt[31]_i_4\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFBFF"
    )
        port map (
      I0 => cnt(3),
      I1 => cnt(2),
      I2 => cnt(1),
      I3 => cnt(0),
      I4 => \cnt[31]_i_9_n_0\,
      O => \cnt[31]_i_4_n_0\
    );
\cnt[31]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => cnt(10),
      I1 => cnt(11),
      I2 => cnt(8),
      I3 => cnt(9),
      I4 => \cnt[31]_i_10_n_0\,
      O => \cnt[31]_i_5_n_0\
    );
\cnt[31]_i_7\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt(21),
      I1 => cnt(20),
      I2 => cnt(23),
      I3 => cnt(22),
      O => \cnt[31]_i_7_n_0\
    );
\cnt[31]_i_8\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt(29),
      I1 => cnt(28),
      I2 => cnt(31),
      I3 => cnt(30),
      O => \cnt[31]_i_8_n_0\
    );
\cnt[31]_i_9\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"FFFE"
    )
        port map (
      I0 => cnt(5),
      I1 => cnt(4),
      I2 => cnt(7),
      I3 => cnt(6),
      O => \cnt[31]_i_9_n_0\
    );
\cnt[3]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(3),
      O => cnt_0(3)
    );
\cnt[4]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(4),
      O => cnt_0(4)
    );
\cnt[5]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(5),
      O => cnt_0(5)
    );
\cnt[6]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(6),
      O => cnt_0(6)
    );
\cnt[7]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(7),
      O => cnt_0(7)
    );
\cnt[8]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(8),
      O => cnt_0(8)
    );
\cnt[9]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFE0000"
    )
        port map (
      I0 => \cnt[31]_i_2_n_0\,
      I1 => \cnt[31]_i_3_n_0\,
      I2 => \cnt[31]_i_4_n_0\,
      I3 => \cnt[31]_i_5_n_0\,
      I4 => data0(9),
      O => cnt_0(9)
    );
\cnt_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(0),
      Q => cnt(0)
    );
\cnt_reg[10]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(10),
      Q => cnt(10)
    );
\cnt_reg[11]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(11),
      Q => cnt(11)
    );
\cnt_reg[12]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(12),
      Q => cnt(12)
    );
\cnt_reg[12]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[8]_i_2_n_0\,
      CO(3) => \cnt_reg[12]_i_2_n_0\,
      CO(2) => \cnt_reg[12]_i_2_n_1\,
      CO(1) => \cnt_reg[12]_i_2_n_2\,
      CO(0) => \cnt_reg[12]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(12 downto 9),
      S(3 downto 0) => cnt(12 downto 9)
    );
\cnt_reg[13]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(13),
      Q => cnt(13)
    );
\cnt_reg[14]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(14),
      Q => cnt(14)
    );
\cnt_reg[15]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(15),
      Q => cnt(15)
    );
\cnt_reg[16]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(16),
      Q => cnt(16)
    );
\cnt_reg[16]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[12]_i_2_n_0\,
      CO(3) => \cnt_reg[16]_i_2_n_0\,
      CO(2) => \cnt_reg[16]_i_2_n_1\,
      CO(1) => \cnt_reg[16]_i_2_n_2\,
      CO(0) => \cnt_reg[16]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(16 downto 13),
      S(3 downto 0) => cnt(16 downto 13)
    );
\cnt_reg[17]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(17),
      Q => cnt(17)
    );
\cnt_reg[18]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(18),
      Q => cnt(18)
    );
\cnt_reg[19]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(19),
      Q => cnt(19)
    );
\cnt_reg[1]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(1),
      Q => cnt(1)
    );
\cnt_reg[20]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(20),
      Q => cnt(20)
    );
\cnt_reg[20]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[16]_i_2_n_0\,
      CO(3) => \cnt_reg[20]_i_2_n_0\,
      CO(2) => \cnt_reg[20]_i_2_n_1\,
      CO(1) => \cnt_reg[20]_i_2_n_2\,
      CO(0) => \cnt_reg[20]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(20 downto 17),
      S(3 downto 0) => cnt(20 downto 17)
    );
\cnt_reg[21]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(21),
      Q => cnt(21)
    );
\cnt_reg[22]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(22),
      Q => cnt(22)
    );
\cnt_reg[23]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(23),
      Q => cnt(23)
    );
\cnt_reg[24]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(24),
      Q => cnt(24)
    );
\cnt_reg[24]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[20]_i_2_n_0\,
      CO(3) => \cnt_reg[24]_i_2_n_0\,
      CO(2) => \cnt_reg[24]_i_2_n_1\,
      CO(1) => \cnt_reg[24]_i_2_n_2\,
      CO(0) => \cnt_reg[24]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(24 downto 21),
      S(3 downto 0) => cnt(24 downto 21)
    );
\cnt_reg[25]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(25),
      Q => cnt(25)
    );
\cnt_reg[26]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(26),
      Q => cnt(26)
    );
\cnt_reg[27]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(27),
      Q => cnt(27)
    );
\cnt_reg[28]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(28),
      Q => cnt(28)
    );
\cnt_reg[28]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[24]_i_2_n_0\,
      CO(3) => \cnt_reg[28]_i_2_n_0\,
      CO(2) => \cnt_reg[28]_i_2_n_1\,
      CO(1) => \cnt_reg[28]_i_2_n_2\,
      CO(0) => \cnt_reg[28]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(28 downto 25),
      S(3 downto 0) => cnt(28 downto 25)
    );
\cnt_reg[29]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(29),
      Q => cnt(29)
    );
\cnt_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(2),
      Q => cnt(2)
    );
\cnt_reg[30]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(30),
      Q => cnt(30)
    );
\cnt_reg[31]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(31),
      Q => cnt(31)
    );
\cnt_reg[31]_i_6\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[28]_i_2_n_0\,
      CO(3 downto 2) => \NLW_cnt_reg[31]_i_6_CO_UNCONNECTED\(3 downto 2),
      CO(1) => \cnt_reg[31]_i_6_n_2\,
      CO(0) => \cnt_reg[31]_i_6_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3) => \NLW_cnt_reg[31]_i_6_O_UNCONNECTED\(3),
      O(2 downto 0) => data0(31 downto 29),
      S(3) => '0',
      S(2 downto 0) => cnt(31 downto 29)
    );
\cnt_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(3),
      Q => cnt(3)
    );
\cnt_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(4),
      Q => cnt(4)
    );
\cnt_reg[4]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => '0',
      CO(3) => \cnt_reg[4]_i_2_n_0\,
      CO(2) => \cnt_reg[4]_i_2_n_1\,
      CO(1) => \cnt_reg[4]_i_2_n_2\,
      CO(0) => \cnt_reg[4]_i_2_n_3\,
      CYINIT => cnt(0),
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(4 downto 1),
      S(3 downto 0) => cnt(4 downto 1)
    );
\cnt_reg[5]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(5),
      Q => cnt(5)
    );
\cnt_reg[6]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(6),
      Q => cnt(6)
    );
\cnt_reg[7]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(7),
      Q => cnt(7)
    );
\cnt_reg[8]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(8),
      Q => cnt(8)
    );
\cnt_reg[8]_i_2\: unisim.vcomponents.CARRY4
     port map (
      CI => \cnt_reg[4]_i_2_n_0\,
      CO(3) => \cnt_reg[8]_i_2_n_0\,
      CO(2) => \cnt_reg[8]_i_2_n_1\,
      CO(1) => \cnt_reg[8]_i_2_n_2\,
      CO(0) => \cnt_reg[8]_i_2_n_3\,
      CYINIT => '0',
      DI(3 downto 0) => B"0000",
      O(3 downto 0) => data0(8 downto 5),
      S(3 downto 0) => cnt(8 downto 5)
    );
\cnt_reg[9]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => reset_IBUF,
      D => cnt_0(9),
      Q => cnt(9)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity ME_cafetera is
  port (
    Q : out STD_LOGIC_VECTOR ( 3 downto 0 );
    indica_on_OBUF : in STD_LOGIC;
    corto_IBUF : in STD_LOGIC;
    largo_IBUF : in STD_LOGIC;
    americano_IBUF : in STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    clear : in STD_LOGIC
  );
end ME_cafetera;

architecture STRUCTURE of ME_cafetera is
  signal \time_req[0]_i_1_n_0\ : STD_LOGIC;
  signal \time_req[2]_i_1_n_0\ : STD_LOGIC;
  signal \time_req[3]_i_1_n_0\ : STD_LOGIC;
  signal \time_req[4]_i_1_n_0\ : STD_LOGIC;
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \time_req[0]_i_1\ : label is "soft_lutpair3";
  attribute SOFT_HLUTNM of \time_req[2]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \time_req[3]_i_1\ : label is "soft_lutpair2";
  attribute SOFT_HLUTNM of \time_req[4]_i_1\ : label is "soft_lutpair3";
begin
\time_req[0]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"0020"
    )
        port map (
      I0 => indica_on_OBUF,
      I1 => corto_IBUF,
      I2 => americano_IBUF,
      I3 => largo_IBUF,
      O => \time_req[0]_i_1_n_0\
    );
\time_req[2]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"2220"
    )
        port map (
      I0 => indica_on_OBUF,
      I1 => corto_IBUF,
      I2 => largo_IBUF,
      I3 => americano_IBUF,
      O => \time_req[2]_i_1_n_0\
    );
\time_req[3]_i_1\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"AA08"
    )
        port map (
      I0 => indica_on_OBUF,
      I1 => americano_IBUF,
      I2 => largo_IBUF,
      I3 => corto_IBUF,
      O => \time_req[3]_i_1_n_0\
    );
\time_req[4]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"20"
    )
        port map (
      I0 => indica_on_OBUF,
      I1 => corto_IBUF,
      I2 => largo_IBUF,
      O => \time_req[4]_i_1_n_0\
    );
\time_req_reg[0]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => clear,
      D => \time_req[0]_i_1_n_0\,
      Q => Q(0)
    );
\time_req_reg[2]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => clear,
      D => \time_req[2]_i_1_n_0\,
      Q => Q(1)
    );
\time_req_reg[3]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => clear,
      D => \time_req[3]_i_1_n_0\,
      Q => Q(2)
    );
\time_req_reg[4]\: unisim.vcomponents.FDCE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      CLR => clear,
      D => \time_req[4]_i_1_n_0\,
      Q => Q(3)
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Sincronos is
  port (
    \one_pipeline.sinc_out_reg_0\ : out STD_LOGIC;
    clk_IBUF_BUFG : in STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 0 to 0 )
  );
end Sincronos;

architecture STRUCTURE of Sincronos is
  signal sinc_out : STD_LOGIC;
  signal sreg : STD_LOGIC_VECTOR ( 2 downto 0 );
  attribute async_reg : string;
  attribute async_reg of sreg : signal is "true";
  attribute ASYNC_REG_boolean : boolean;
  attribute ASYNC_REG_boolean of \sreg_reg[0]\ : label is std.standard.true;
  attribute KEEP : string;
  attribute KEEP of \sreg_reg[0]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sreg_reg[1]\ : label is std.standard.true;
  attribute KEEP of \sreg_reg[1]\ : label is "yes";
  attribute ASYNC_REG_boolean of \sreg_reg[2]\ : label is std.standard.true;
  attribute KEEP of \sreg_reg[2]\ : label is "yes";
begin
Q1_i_1: unisim.vcomponents.LUT2
    generic map(
      INIT => X"8"
    )
        port map (
      I0 => sinc_out,
      I1 => reset_IBUF,
      O => \one_pipeline.sinc_out_reg_0\
    );
\one_pipeline.sinc_out_reg\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(2),
      Q => sinc_out,
      R => '0'
    );
\sreg_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => D(0),
      Q => sreg(0),
      R => '0'
    );
\sreg_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(0),
      Q => sreg(1),
      R => '0'
    );
\sreg_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_IBUF_BUFG,
      CE => '1',
      D => sreg(1),
      Q => sreg(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Temporizador is
  port (
    clear : out STD_LOGIC;
    bomba_OBUF : out STD_LOGIC;
    D : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \FSM_onehot_state_reg[1]\ : out STD_LOGIC;
    \FSM_onehot_state_reg[0]\ : out STD_LOGIC;
    \FSM_onehot_state_reg[1]_0\ : out STD_LOGIC;
    \time_required_i_reg[1]_0\ : out STD_LOGIC;
    \time_required_i_reg[0]_0\ : out STD_LOGIC;
    \time_required_i_reg[1]_1\ : out STD_LOGIC;
    \FSM_onehot_state_reg[1]_1\ : out STD_LOGIC;
    \time_required_i_reg[4]_0\ : out STD_LOGIC;
    reset_IBUF : in STD_LOGIC;
    Q : in STD_LOGIC_VECTOR ( 3 downto 0 );
    \display_number_reg[0]\ : in STD_LOGIC;
    \display_number_reg[1]\ : in STD_LOGIC;
    \display_number_reg[3]\ : in STD_LOGIC_VECTOR ( 1 downto 0 );
    \display_number_reg[6]\ : in STD_LOGIC;
    clk_divided : in STD_LOGIC
  );
end Temporizador;

architecture STRUCTURE of Temporizador is
  signal \^fsm_onehot_state_reg[0]\ : STD_LOGIC;
  signal \^fsm_onehot_state_reg[1]\ : STD_LOGIC;
  signal \^clear\ : STD_LOGIC;
  signal \display_number[6]_i_3_n_0\ : STD_LOGIC;
  signal \display_number[6]_i_4_n_0\ : STD_LOGIC;
  signal p_0_in : STD_LOGIC_VECTOR ( 4 downto 0 );
  signal time_required_i_reg : STD_LOGIC_VECTOR ( 4 downto 0 );
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of bomba_OBUF_inst_i_1 : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \display_number[2]_i_3\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \display_number[4]_i_3\ : label is "soft_lutpair6";
  attribute SOFT_HLUTNM of \display_number[5]_i_2\ : label is "soft_lutpair4";
  attribute SOFT_HLUTNM of \display_number[6]_i_3\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \display_number[6]_i_4\ : label is "soft_lutpair7";
  attribute SOFT_HLUTNM of \display_number[6]_i_5\ : label is "soft_lutpair5";
  attribute SOFT_HLUTNM of \display_number[6]_i_7\ : label is "soft_lutpair7";
begin
  \FSM_onehot_state_reg[0]\ <= \^fsm_onehot_state_reg[0]\;
  \FSM_onehot_state_reg[1]\ <= \^fsm_onehot_state_reg[1]\;
  clear <= \^clear\;
bomba_OBUF_inst_i_1: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFFFFFE"
    )
        port map (
      I0 => time_required_i_reg(4),
      I1 => time_required_i_reg(3),
      I2 => time_required_i_reg(2),
      I3 => time_required_i_reg(1),
      I4 => time_required_i_reg(0),
      O => bomba_OBUF
    );
\display_number[0]_i_1\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0062"
    )
        port map (
      I0 => \display_number[6]_i_3_n_0\,
      I1 => \display_number[6]_i_4_n_0\,
      I2 => time_required_i_reg(0),
      I3 => \^fsm_onehot_state_reg[1]\,
      I4 => \display_number_reg[0]\,
      O => D(0)
    );
\display_number[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFFFFFF0E08"
    )
        port map (
      I0 => \display_number[6]_i_4_n_0\,
      I1 => time_required_i_reg(0),
      I2 => \^fsm_onehot_state_reg[1]\,
      I3 => \display_number[6]_i_3_n_0\,
      I4 => \display_number_reg[1]\,
      I5 => \^fsm_onehot_state_reg[0]\,
      O => D(1)
    );
\display_number[2]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8888A8888A8888A8"
    )
        port map (
      I0 => \display_number_reg[3]\(1),
      I1 => time_required_i_reg(0),
      I2 => time_required_i_reg(2),
      I3 => time_required_i_reg(3),
      I4 => time_required_i_reg(4),
      I5 => time_required_i_reg(1),
      O => \FSM_onehot_state_reg[1]_1\
    );
\display_number[2]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"82A00280"
    )
        port map (
      I0 => \display_number_reg[3]\(0),
      I1 => time_required_i_reg(2),
      I2 => time_required_i_reg(3),
      I3 => time_required_i_reg(4),
      I4 => time_required_i_reg(1),
      O => \^fsm_onehot_state_reg[0]\
    );
\display_number[3]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"8800288002880028"
    )
        port map (
      I0 => \display_number_reg[3]\(1),
      I1 => time_required_i_reg(0),
      I2 => time_required_i_reg(2),
      I3 => time_required_i_reg(3),
      I4 => time_required_i_reg(4),
      I5 => time_required_i_reg(1),
      O => \FSM_onehot_state_reg[1]_0\
    );
\display_number[4]_i_2\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"0401004000000000"
    )
        port map (
      I0 => time_required_i_reg(0),
      I1 => time_required_i_reg(2),
      I2 => time_required_i_reg(3),
      I3 => time_required_i_reg(4),
      I4 => time_required_i_reg(1),
      I5 => \display_number_reg[3]\(1),
      O => \time_required_i_reg[0]_0\
    );
\display_number[4]_i_3\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"4CC00000"
    )
        port map (
      I0 => time_required_i_reg(1),
      I1 => time_required_i_reg(4),
      I2 => time_required_i_reg(3),
      I3 => time_required_i_reg(2),
      I4 => \display_number_reg[3]\(0),
      O => \time_required_i_reg[1]_0\
    );
\display_number[5]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"21400284"
    )
        port map (
      I0 => time_required_i_reg(1),
      I1 => time_required_i_reg(4),
      I2 => time_required_i_reg(3),
      I3 => time_required_i_reg(2),
      I4 => time_required_i_reg(0),
      O => \time_required_i_reg[1]_1\
    );
\display_number[6]_i_2\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"FFFF0021"
    )
        port map (
      I0 => \display_number[6]_i_3_n_0\,
      I1 => \display_number[6]_i_4_n_0\,
      I2 => time_required_i_reg(0),
      I3 => \^fsm_onehot_state_reg[1]\,
      I4 => \display_number_reg[6]\,
      O => D(2)
    );
\display_number[6]_i_3\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"DC3B"
    )
        port map (
      I0 => time_required_i_reg(1),
      I1 => time_required_i_reg(4),
      I2 => time_required_i_reg(3),
      I3 => time_required_i_reg(2),
      O => \display_number[6]_i_3_n_0\
    );
\display_number[6]_i_4\: unisim.vcomponents.LUT4
    generic map(
      INIT => X"6318"
    )
        port map (
      I0 => time_required_i_reg(2),
      I1 => time_required_i_reg(3),
      I2 => time_required_i_reg(4),
      I3 => time_required_i_reg(1),
      O => \display_number[6]_i_4_n_0\
    );
\display_number[6]_i_5\: unisim.vcomponents.LUT5
    generic map(
      INIT => X"5755D55D"
    )
        port map (
      I0 => \display_number_reg[3]\(1),
      I1 => time_required_i_reg(3),
      I2 => time_required_i_reg(2),
      I3 => time_required_i_reg(4),
      I4 => time_required_i_reg(1),
      O => \^fsm_onehot_state_reg[1]\
    );
\display_number[6]_i_7\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"A8"
    )
        port map (
      I0 => time_required_i_reg(4),
      I1 => time_required_i_reg(3),
      I2 => time_required_i_reg(2),
      O => \time_required_i_reg[4]_0\
    );
\time_req[4]_i_2\: unisim.vcomponents.LUT1
    generic map(
      INIT => X"1"
    )
        port map (
      I0 => reset_IBUF,
      O => \^clear\
    );
\time_required_i[0]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"3333333333333332"
    )
        port map (
      I0 => Q(0),
      I1 => time_required_i_reg(0),
      I2 => time_required_i_reg(1),
      I3 => time_required_i_reg(2),
      I4 => time_required_i_reg(3),
      I5 => time_required_i_reg(4),
      O => p_0_in(0)
    );
\time_required_i[1]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"C3C3C3C3C3C3C3C2"
    )
        port map (
      I0 => Q(2),
      I1 => time_required_i_reg(0),
      I2 => time_required_i_reg(1),
      I3 => time_required_i_reg(2),
      I4 => time_required_i_reg(3),
      I5 => time_required_i_reg(4),
      O => p_0_in(1)
    );
\time_required_i[2]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FC03FC03FC03FC02"
    )
        port map (
      I0 => Q(1),
      I1 => time_required_i_reg(0),
      I2 => time_required_i_reg(1),
      I3 => time_required_i_reg(2),
      I4 => time_required_i_reg(3),
      I5 => time_required_i_reg(4),
      O => p_0_in(2)
    );
\time_required_i[3]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFC0003FFFC0002"
    )
        port map (
      I0 => Q(2),
      I1 => time_required_i_reg(0),
      I2 => time_required_i_reg(1),
      I3 => time_required_i_reg(2),
      I4 => time_required_i_reg(3),
      I5 => time_required_i_reg(4),
      O => p_0_in(3)
    );
\time_required_i[4]_i_1\: unisim.vcomponents.LUT6
    generic map(
      INIT => X"FFFFFFFC00000002"
    )
        port map (
      I0 => Q(3),
      I1 => time_required_i_reg(0),
      I2 => time_required_i_reg(1),
      I3 => time_required_i_reg(2),
      I4 => time_required_i_reg(3),
      I5 => time_required_i_reg(4),
      O => p_0_in(4)
    );
\time_required_i_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => '1',
      D => p_0_in(0),
      Q => time_required_i_reg(0),
      R => \^clear\
    );
\time_required_i_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => '1',
      D => p_0_in(1),
      Q => time_required_i_reg(1),
      R => \^clear\
    );
\time_required_i_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => '1',
      D => p_0_in(2),
      Q => time_required_i_reg(2),
      R => \^clear\
    );
\time_required_i_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => '1',
      D => p_0_in(3),
      Q => time_required_i_reg(3),
      R => \^clear\
    );
\time_required_i_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => '1',
      D => p_0_in(4),
      Q => time_required_i_reg(4),
      R => \^clear\
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity display_refresh is
  port (
    Q : out STD_LOGIC_VECTOR ( 2 downto 0 );
    \display_number_reg[6]_0\ : out STD_LOGIC_VECTOR ( 6 downto 0 );
    \display_timer_selection_reg[3]_0\ : out STD_LOGIC_VECTOR ( 2 downto 0 );
    clk_divided : in STD_LOGIC;
    D : in STD_LOGIC_VECTOR ( 6 downto 0 )
  );
end display_refresh;

architecture STRUCTURE of display_refresh is
  signal \^q\ : STD_LOGIC_VECTOR ( 2 downto 0 );
  signal \display_number[6]_i_1_n_0\ : STD_LOGIC;
  signal \display_timer_selection[0]_i_1_n_0\ : STD_LOGIC;
  signal \display_timer_selection[2]_i_1_n_0\ : STD_LOGIC;
  signal \display_timer_selection[3]_i_1_n_0\ : STD_LOGIC;
  attribute FSM_ENCODED_STATES : string;
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[0]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:100,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[1]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:100,";
  attribute FSM_ENCODED_STATES of \FSM_onehot_state_reg[2]\ : label is "iSTATE:001,iSTATE0:010,iSTATE1:100,";
  attribute SOFT_HLUTNM : string;
  attribute SOFT_HLUTNM of \display_timer_selection[0]_i_1\ : label is "soft_lutpair8";
  attribute SOFT_HLUTNM of \display_timer_selection[3]_i_1\ : label is "soft_lutpair8";
begin
  Q(2 downto 0) <= \^q\(2 downto 0);
\FSM_onehot_state_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '1'
    )
        port map (
      C => clk_divided,
      CE => '1',
      D => \^q\(2),
      Q => \^q\(0),
      R => '0'
    );
\FSM_onehot_state_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => '1',
      D => \^q\(0),
      Q => \^q\(1),
      R => '0'
    );
\FSM_onehot_state_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => '1',
      D => \^q\(1),
      Q => \^q\(2),
      R => '0'
    );
\display_number[6]_i_1\: unisim.vcomponents.LUT3
    generic map(
      INIT => X"FE"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      I2 => \^q\(0),
      O => \display_number[6]_i_1_n_0\
    );
\display_number_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => D(0),
      Q => \display_number_reg[6]_0\(0),
      R => '0'
    );
\display_number_reg[1]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => D(1),
      Q => \display_number_reg[6]_0\(1),
      R => '0'
    );
\display_number_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => D(2),
      Q => \display_number_reg[6]_0\(2),
      R => '0'
    );
\display_number_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => D(3),
      Q => \display_number_reg[6]_0\(3),
      R => '0'
    );
\display_number_reg[4]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => D(4),
      Q => \display_number_reg[6]_0\(4),
      R => '0'
    );
\display_number_reg[5]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => D(5),
      Q => \display_number_reg[6]_0\(5),
      R => '0'
    );
\display_number_reg[6]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => D(6),
      Q => \display_number_reg[6]_0\(6),
      R => '0'
    );
\display_timer_selection[0]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(1),
      O => \display_timer_selection[0]_i_1_n_0\
    );
\display_timer_selection[2]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(2),
      I1 => \^q\(1),
      O => \display_timer_selection[2]_i_1_n_0\
    );
\display_timer_selection[3]_i_1\: unisim.vcomponents.LUT2
    generic map(
      INIT => X"E"
    )
        port map (
      I0 => \^q\(0),
      I1 => \^q\(2),
      O => \display_timer_selection[3]_i_1_n_0\
    );
\display_timer_selection_reg[0]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => \display_timer_selection[0]_i_1_n_0\,
      Q => \display_timer_selection_reg[3]_0\(0),
      R => '0'
    );
\display_timer_selection_reg[2]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => \display_timer_selection[2]_i_1_n_0\,
      Q => \display_timer_selection_reg[3]_0\(1),
      R => '0'
    );
\display_timer_selection_reg[3]\: unisim.vcomponents.FDRE
    generic map(
      INIT => '0'
    )
        port map (
      C => clk_divided,
      CE => \display_number[6]_i_1_n_0\,
      D => \display_timer_selection[3]_i_1_n_0\,
      Q => \display_timer_selection_reg[3]_0\(2),
      R => '0'
    );
end STRUCTURE;
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity Top is
  port (
    clk : in STD_LOGIC;
    reset : in STD_LOGIC;
    corto : in STD_LOGIC;
    largo : in STD_LOGIC;
    americano : in STD_LOGIC;
    Pulsador_on : in STD_LOGIC;
    sugar : in STD_LOGIC;
    refill : in STD_LOGIC;
    display_number : out STD_LOGIC_VECTOR ( 6 downto 0 );
    display_timer_selection : out STD_LOGIC_VECTOR ( 7 downto 0 );
    bomba : out STD_LOGIC;
    indica_on : out STD_LOGIC;
    Despensa_led : out STD_LOGIC
  );
  attribute NotValidForBitStream : boolean;
  attribute NotValidForBitStream of Top : entity is true;
end Top;

architecture STRUCTURE of Top is
  signal Despensa_led_OBUF : STD_LOGIC;
  signal Q1 : STD_LOGIC;
  signal Q2 : STD_LOGIC;
  signal Q3 : STD_LOGIC;
  signal americano_IBUF : STD_LOGIC;
  signal aux_level : STD_LOGIC;
  signal bomba_OBUF : STD_LOGIC;
  signal clear : STD_LOGIC;
  signal clk_IBUF : STD_LOGIC;
  signal clk_IBUF_BUFG : STD_LOGIC;
  signal clk_divided : STD_LOGIC;
  signal corto_IBUF : STD_LOGIC;
  signal display_number_OBUF : STD_LOGIC_VECTOR ( 6 downto 0 );
  signal display_timer_selection_OBUF : STD_LOGIC_VECTOR ( 3 downto 0 );
  signal indica_on_OBUF : STD_LOGIC;
  signal inst_Despensa_n_1 : STD_LOGIC;
  signal inst_Despensa_n_2 : STD_LOGIC;
  signal inst_Despensa_n_3 : STD_LOGIC;
  signal inst_Despensa_n_4 : STD_LOGIC;
  signal inst_Despensa_n_5 : STD_LOGIC;
  signal inst_Despensa_n_6 : STD_LOGIC;
  signal inst_Despensa_n_7 : STD_LOGIC;
  signal inst_Me_cafetera_n_0 : STD_LOGIC;
  signal inst_Me_cafetera_n_1 : STD_LOGIC;
  signal inst_Me_cafetera_n_2 : STD_LOGIC;
  signal inst_Me_cafetera_n_3 : STD_LOGIC;
  signal inst_Temporizador_n_10 : STD_LOGIC;
  signal inst_Temporizador_n_11 : STD_LOGIC;
  signal inst_Temporizador_n_12 : STD_LOGIC;
  signal inst_Temporizador_n_2 : STD_LOGIC;
  signal inst_Temporizador_n_3 : STD_LOGIC;
  signal inst_Temporizador_n_4 : STD_LOGIC;
  signal inst_Temporizador_n_5 : STD_LOGIC;
  signal inst_Temporizador_n_6 : STD_LOGIC;
  signal inst_Temporizador_n_7 : STD_LOGIC;
  signal inst_Temporizador_n_8 : STD_LOGIC;
  signal inst_Temporizador_n_9 : STD_LOGIC;
  signal inst_refresh_n_0 : STD_LOGIC;
  signal inst_refresh_n_1 : STD_LOGIC;
  signal inst_refresh_n_2 : STD_LOGIC;
  signal inst_sync_sugar_n_0 : STD_LOGIC;
  signal largo_IBUF : STD_LOGIC;
  signal refill_IBUF : STD_LOGIC;
  signal reset_IBUF : STD_LOGIC;
  signal sugar_IBUF : STD_LOGIC;
begin
Despensa_led_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => Despensa_led_OBUF,
      O => Despensa_led
    );
Pulsador_on_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => Pulsador_on,
      O => indica_on_OBUF
    );
americano_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => americano,
      O => americano_IBUF
    );
bomba_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => bomba_OBUF,
      O => bomba
    );
clk_IBUF_BUFG_inst: unisim.vcomponents.BUFG
     port map (
      I => clk_IBUF,
      O => clk_IBUF_BUFG
    );
clk_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => clk,
      O => clk_IBUF
    );
corto_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => corto,
      O => corto_IBUF
    );
\display_number_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_number_OBUF(0),
      O => display_number(0)
    );
\display_number_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_number_OBUF(1),
      O => display_number(1)
    );
\display_number_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_number_OBUF(2),
      O => display_number(2)
    );
\display_number_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_number_OBUF(3),
      O => display_number(3)
    );
\display_number_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_number_OBUF(4),
      O => display_number(4)
    );
\display_number_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_number_OBUF(5),
      O => display_number(5)
    );
\display_number_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_number_OBUF(6),
      O => display_number(6)
    );
\display_timer_selection_OBUF[0]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_timer_selection_OBUF(0),
      O => display_timer_selection(0)
    );
\display_timer_selection_OBUF[1]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_timer_selection(1)
    );
\display_timer_selection_OBUF[2]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_timer_selection_OBUF(2),
      O => display_timer_selection(2)
    );
\display_timer_selection_OBUF[3]_inst\: unisim.vcomponents.OBUF
     port map (
      I => display_timer_selection_OBUF(3),
      O => display_timer_selection(3)
    );
\display_timer_selection_OBUF[4]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_timer_selection(4)
    );
\display_timer_selection_OBUF[5]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_timer_selection(5)
    );
\display_timer_selection_OBUF[6]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_timer_selection(6)
    );
\display_timer_selection_OBUF[7]_inst\: unisim.vcomponents.OBUF
     port map (
      I => '1',
      O => display_timer_selection(7)
    );
indica_on_OBUF_inst: unisim.vcomponents.OBUF
     port map (
      I => indica_on_OBUF,
      O => indica_on
    );
inst_Antirrebote_Azucar: entity work.Antirrebote
     port map (
      Q1 => Q1,
      Q1_reg_0 => inst_sync_sugar_n_0,
      Q2 => Q2,
      Q3 => Q3,
      SR(0) => refill_IBUF,
      aux_level => aux_level,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      reset_IBUF => reset_IBUF
    );
inst_Despensa: entity work.Despensa
     port map (
      D(3) => inst_Despensa_n_1,
      D(2) => inst_Despensa_n_2,
      D(1) => inst_Despensa_n_3,
      D(0) => inst_Despensa_n_4,
      Despensa_led_OBUF => Despensa_led_OBUF,
      \FSM_onehot_state_reg[0]\ => inst_Despensa_n_6,
      \FSM_onehot_state_reg[0]_0\ => inst_Despensa_n_7,
      Q(1) => inst_refresh_n_0,
      Q(0) => inst_refresh_n_2,
      Q1 => Q1,
      Q2 => Q2,
      Q3 => Q3,
      SR(0) => refill_IBUF,
      aux_level => aux_level,
      \aux_level_reg[0]_0\ => inst_Despensa_n_5,
      bomba_OBUF => bomba_OBUF,
      clear => clear,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \display_number_reg[1]\ => inst_Temporizador_n_12,
      \display_number_reg[2]\ => inst_Temporizador_n_11,
      \display_number_reg[2]_0\ => inst_Temporizador_n_6,
      \display_number_reg[3]\ => inst_Temporizador_n_7,
      \display_number_reg[4]\ => inst_Temporizador_n_9,
      \display_number_reg[4]_0\ => inst_Temporizador_n_8,
      \display_number_reg[5]\ => inst_Temporizador_n_5,
      \display_number_reg[5]_0\ => inst_Temporizador_n_10,
      reset_IBUF => reset_IBUF
    );
inst_Div_Frecuencia_general: entity work.Div_Frecuencia
     port map (
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      clk_divided => clk_divided,
      reset_IBUF => reset_IBUF
    );
inst_Me_cafetera: entity work.ME_cafetera
     port map (
      Q(3) => inst_Me_cafetera_n_0,
      Q(2) => inst_Me_cafetera_n_1,
      Q(1) => inst_Me_cafetera_n_2,
      Q(0) => inst_Me_cafetera_n_3,
      americano_IBUF => americano_IBUF,
      clear => clear,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      corto_IBUF => corto_IBUF,
      indica_on_OBUF => indica_on_OBUF,
      largo_IBUF => largo_IBUF
    );
inst_Temporizador: entity work.Temporizador
     port map (
      D(2) => inst_Temporizador_n_2,
      D(1) => inst_Temporizador_n_3,
      D(0) => inst_Temporizador_n_4,
      \FSM_onehot_state_reg[0]\ => inst_Temporizador_n_6,
      \FSM_onehot_state_reg[1]\ => inst_Temporizador_n_5,
      \FSM_onehot_state_reg[1]_0\ => inst_Temporizador_n_7,
      \FSM_onehot_state_reg[1]_1\ => inst_Temporizador_n_11,
      Q(3) => inst_Me_cafetera_n_0,
      Q(2) => inst_Me_cafetera_n_1,
      Q(1) => inst_Me_cafetera_n_2,
      Q(0) => inst_Me_cafetera_n_3,
      bomba_OBUF => bomba_OBUF,
      clear => clear,
      clk_divided => clk_divided,
      \display_number_reg[0]\ => inst_Despensa_n_7,
      \display_number_reg[1]\ => inst_Despensa_n_6,
      \display_number_reg[3]\(1) => inst_refresh_n_1,
      \display_number_reg[3]\(0) => inst_refresh_n_2,
      \display_number_reg[6]\ => inst_Despensa_n_5,
      reset_IBUF => reset_IBUF,
      \time_required_i_reg[0]_0\ => inst_Temporizador_n_9,
      \time_required_i_reg[1]_0\ => inst_Temporizador_n_8,
      \time_required_i_reg[1]_1\ => inst_Temporizador_n_10,
      \time_required_i_reg[4]_0\ => inst_Temporizador_n_12
    );
inst_refresh: entity work.display_refresh
     port map (
      D(6) => inst_Temporizador_n_2,
      D(5) => inst_Despensa_n_1,
      D(4) => inst_Despensa_n_2,
      D(3) => inst_Despensa_n_3,
      D(2) => inst_Despensa_n_4,
      D(1) => inst_Temporizador_n_3,
      D(0) => inst_Temporizador_n_4,
      Q(2) => inst_refresh_n_0,
      Q(1) => inst_refresh_n_1,
      Q(0) => inst_refresh_n_2,
      clk_divided => clk_divided,
      \display_number_reg[6]_0\(6 downto 0) => display_number_OBUF(6 downto 0),
      \display_timer_selection_reg[3]_0\(2 downto 1) => display_timer_selection_OBUF(3 downto 2),
      \display_timer_selection_reg[3]_0\(0) => display_timer_selection_OBUF(0)
    );
inst_sync_sugar: entity work.Sincronos
     port map (
      D(0) => sugar_IBUF,
      clk_IBUF_BUFG => clk_IBUF_BUFG,
      \one_pipeline.sinc_out_reg_0\ => inst_sync_sugar_n_0,
      reset_IBUF => reset_IBUF
    );
largo_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => largo,
      O => largo_IBUF
    );
refill_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => refill,
      O => refill_IBUF
    );
reset_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => reset,
      O => reset_IBUF
    );
sugar_IBUF_inst: unisim.vcomponents.IBUF
     port map (
      I => sugar,
      O => sugar_IBUF
    );
end STRUCTURE;
