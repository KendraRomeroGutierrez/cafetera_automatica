library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Display_tb is
end Display_tb;

architecture Behavioral of Display_tb is

component Display is
	port(	display1,display2,display3,display4, display5,display6,display7,display8: in std_logic_vector(7 downto 0);
			clk: in std_logic;
			reset: in std_logic;
			caracter: out std_logic_vector(7 downto 0);
			control_display: out std_logic_vector(7 downto 0));
end component;
-- Mapeo de Se�ales 
            signal display1_s: std_logic_vector(7 downto 0) := (others => '0');
            signal display2_s: std_logic_vector(7 downto 0) := (others => '0');
            signal display3_s: std_logic_vector(7 downto 0) := (others => '0');
            signal display4_s: std_logic_vector(7 downto 0) := (others => '0');
            signal display5_s: std_logic_vector(7 downto 0) := (others => '0');
            signal display6_s: std_logic_vector(7 downto 0) := (others => '0');
            signal display7_s: std_logic_vector(7 downto 0) := (others => '0');
            signal display8_s: std_logic_vector(7 downto 0) := (others => '0');
            signal clk_s: std_logic:='0';
            signal reset_s: std_logic:= '0';
            
            signal caracter: std_logic_vector(7 downto 0);
            signal control_display: std_logic_vector(7 downto 0);

begin

    UUT: Display port map(
          display1 => display1_s,
          display2 => display2_s,
          display3 => display3_s,
          display4 => display4_s,
          display5 => display5_s,
          display6 => display6_s,
          display7 => display7_s,
          display8 => display8_s,
          clk => clk_S,
          reset=> reset_s,
          caracter => caracter,
          control_display =>control_display);
 
  
  clk_p:process
   begin
		clk_S <= '0';
		wait for 10ns;
		clk_S <= '1';
		wait for 10ns;
   end process;
  
   Salida_ME_Cafetera: process
   begin		

		wait for 50 ns; -- comprobamos que se forme la palabra Cafetera
		display1_s<= "11000110"; --C
		display2_s<= "10001000"; --A
		display3_s<= "10001110"; --F
		display4_s<= "10000110"; --E
		display5_s<= "10000111"; --t
		display6_s<= "10000110"; --E
		display7_s<= "10101111"; --r
		display8_s<= "10001000"; --A
      wait;
   end process;
   
    

end Behavioral;
