library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Div_Frecuencia_tb is
end Div_Frecuencia_tb;

architecture Behavioral of Div_Frecuencia_tb is
component Div_Frecuencia 
    Port ( 
           clk : in  STD_LOGIC; -- reloj
           reset : in  STD_LOGIC; -- reset 
           clk_out : out  STD_LOGIC); -- pulso con duracion de un ciclo
end component;

    signal clk_s: std_logic;
    signal reset_s: std_logic;
    signal clk_out_s: std_logic;
begin
-- mapeamos las se�ales 
    prueba: Div_Frecuencia port map(
                clk=> clk_s,
                reset=> reset_s,
                clk_out=> clk_out_s); 
    process
     begin
            clk_S <='0';
            reset_s <= '0';
            
            wait for 10ns;
            reset_s <= '0';
            

end process;

end Behavioral;
