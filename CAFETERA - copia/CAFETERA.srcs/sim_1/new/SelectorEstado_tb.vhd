library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity SelectorEstado_tb is
end SelectorEstado_tb;

architecture Behavioral of SelectorEstado_tb is

component SelectorEstado is
      port(
      clk_SE: in std_logic;
	  salida_SE: out std_logic_vector(2 downto 0) );
	  end component;
--Una se�al para cada puerto del componente
    signal clk_SE_s: std_logic;
    signal salida_SE_s: std_logic_vector(2 downto 0);
begin
--instancia
    UUT: SelectorEstado port map(
    clk_SE=> clk_SE_s,
    salida_SE => salida_SE_s
    ); 
-- generacion de estimulo
estimulo: process
begin
     clk_SE_s <= '1';
     wait for 10ns;
     clk_SE_s <= '0';
     wait for 10ns;
     clk_SE_s <= '1';
     
end process;
end Behavioral;
