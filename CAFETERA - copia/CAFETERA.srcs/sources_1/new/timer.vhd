library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity Temporizador is
    Port ( 
        clk, reset: in STD_LOGIC;
        time_required: in STD_LOGIC_VECTOR (4 DOWNTO 0);
        segment_unit, segment_ten: out STD_LOGIC_VECTOR (3 DOWNTO 0);
        time_count: out STD_LOGIC_VECTOR (4 DOWNTO 0)
    );
end Temporizador;

architecture Behavioral of Temporizador is
    
    signal time_required_i:unsigned(4 downto 0):= unsigned(time_required);
    
begin
    
    process(clk)
        variable decenas: natural;
        variable unidades: natural;
        begin
        if rising_edge(clk) then
            if reset = '0' then
                time_required_i <= (others => '0');
            else
                if time_required_i > 0 then
                    time_required_i <= time_required_i - 1;
                elsif time_required_i = 0 then
                    time_required_i <= unsigned(time_required);
                end if;
            end if;
        end if;
        
        time_count <= std_logic_vector(time_required_i);
        
        decenas := to_integer(time_required_i/10);
        unidades := to_integer(time_required_i mod 10);
        
        segment_unit <= std_logic_vector(to_unsigned(unidades , 4));
        segment_ten <= std_logic_vector(to_unsigned(decenas , 4));   
    
    end process;    
            
end Behavioral;