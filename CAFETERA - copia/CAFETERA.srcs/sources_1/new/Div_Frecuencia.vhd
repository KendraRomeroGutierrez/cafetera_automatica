-- la salida produce un pulso con duracion clk_out
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
entity Div_Frecuencia is
    Generic (frec: integer:=5);  -- Por defecto 
    Port ( 
           clk : in  STD_LOGIC; -- reloj
           reset : in  STD_LOGIC; -- reset 
           clk_out : out  STD_LOGIC); -- pulso con duracion de un ciclo
end Div_Frecuencia;
architecture Behavioral of Div_Frecuencia is
  subtype contador_t is natural range 0 to frec;
  signal contador: contador_t;  --Se�al "contador" con valor inicial 0		
begin
  divisor:	process(clk,reset)
  begin
    if reset = '1' then  --Si reset vale 1
      contador <= 0;  -- el contador vale 0
      clk_out <= '0';  --A la se�al de salida se le asigna el valor 0
    elsif clk'event and clk = '1' then  --Si hay un flanco positivo en la entrada clk o clk=1
      if contador >= frec - 1 then  --Si la se�al "contador" es mayor o igual que max-1=9
        clk_out <= '1';  --A la se�al de salida se le asigna el valor 1
        contador <= 0;  --A la se�al "contador" se le asigna el valor 0
      else  --Si no se cumple ninguno de los dos casos anteriores
        clk_out <= '0';  -- a la salida se le asigna el valor 0
        contador <=contador + 1;  --contador incrementa en 1 su valor
      end if;
    end if;
  end process;

end Behavioral;


