library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity display_refresh is
    generic(refresh: time:=9ms); -- El ciclo de reloj es 50 Hz (se actualiza cada 20 ms)
    Port ( 
        ten_decoded, unit_decoded, sugar_decoded: in STD_LOGIC_VECTOR (6 DOWNTO 0);
        clk: in STD_LOGIC;
        display_number: out STD_LOGIC_VECTOR (6 DOWNTO 0);
        display_timer_selection: out STD_LOGIC_VECTOR (7 DOWNTO 0)
    );
end display_refresh;

architecture Behavioral of display_refresh is
    signal state : std_logic_vector (1 downto 0):="00";
begin
    process(clk)
    begin
        if rising_edge(clk) then
            case state is
                when "00" => 
                    display_timer_selection <= "11111011";
                    display_number <= ten_decoded;
                    state <= "01"; -- encendido
                when "01" =>
                    display_timer_selection <= "11110111"; 
                    display_number <= unit_decoded;
                    state <= "10"; -- corto / largo
                when "10" =>
                    display_timer_selection <= "11111110"; 
                    display_number <= sugar_decoded;
                    state <= "00"; -- apagado
                when others =>
                    state <= "00";
            end case;
        end if;
    end process;
    

end Behavioral;

